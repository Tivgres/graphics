package me.tivgres;

import javafx.application.Application;
import javafx.stage.Stage;
import me.tivgres.drawer.INavigation;
import me.tivgres.drawer.Primary;
import me.tivgres.drawer.line.LineDrawerView;
import me.tivgres.drawer.round.RoundDrawerView;
import me.tivgres.drawer.polygon.PolygonFillView;

public class Main extends Application implements INavigation {

    private Stage primaryStage;

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        prepareView();
    }

    private void prepareView(){
        primaryStage.setTitle("Drawer");
        primaryStage.setMinWidth(1000);
        primaryStage.setWidth(1000);
        primaryStage.setMinHeight(700);
        primaryStage.setHeight(700);
        primaryStage.resizableProperty().setValue(false);
        this.showPrimary(primaryStage);
    }

    public void showPrimary(Stage stage) {
        new Primary(primaryStage, this);
    }

    public void drawLine(Stage stage) {
        new LineDrawerView(primaryStage, this);
    }

    public void drawRound(Stage stage) {
        new RoundDrawerView(primaryStage, this);
    }

    public void fillRound(Stage stage) {
        new PolygonFillView(primaryStage, this);
    }
}
