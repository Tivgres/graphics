package me.tivgres.drawer.round;

import javafx.scene.image.PixelWriter;
import javafx.scene.paint.Color;

public class RoundDrawer {

    RoundDrawer(PixelWriter pw, Color lineColor, int diameter) {
        // R - радиус, X1, Y1 - координаты центра
        int X1 = 100;
        int Y1 = 100;
        int x = 0;
        int y = diameter / 2;
        int delta = 1 - 2 * y;
        int error = 0;
        while (y >= 0) {
            pw.setColor(X1 + x, Y1 + y, lineColor);
            pw.setColor(X1 + x, Y1 - y, lineColor);
            pw.setColor(X1 - x, Y1 + y, lineColor);
            pw.setColor(X1 - x, Y1 - y, lineColor);
            error = 2 * (delta + y) - 1;
            if ((delta < 0) && (error <= 0)) {
                delta += 2 * ++x + 1;
                continue;
            }
            error = 2 * (delta - x) - 1;
            if ((delta > 0) && (error > 0)) {
                delta += 1 - 2 * --y;
                continue;
            }
            x++;
            delta += 2 * (x - y);
            y--;
        }
    }
}
