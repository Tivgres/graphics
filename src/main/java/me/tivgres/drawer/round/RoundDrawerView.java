package me.tivgres.drawer.round;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.image.PixelWriter;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import me.tivgres.base.BaseView;
import me.tivgres.drawer.INavigation;


public class RoundDrawerView extends BaseView {

    private INavigation INavigation;
    private Stage stage;
    private BorderPane mainPane = new BorderPane();
    private TextField fieldDiameter = new TextField();
    private Button
            btnMainView = new Button("<"),
            btnDrawRound = new Button("Draw");

    public RoundDrawerView(Stage stage, INavigation INavigation){
        this.INavigation = INavigation;
        this.stage = stage;
        prepareWorkspace();
        showScene();
    }

    @Override
    protected void prepareWorkspace() {
        mainPane.setPadding(new Insets(25));
        mainPane.setBackground(super.getBackground());
        BorderPane topView = getTopView();
        topView.setPrefWidth(1000);
        BorderPane.setAlignment(topView, Pos.TOP_CENTER);
        mainPane.setTop(topView);
        BorderPane centerView = getCenterView();
        BorderPane.setAlignment(centerView, Pos.CENTER);
        mainPane.setCenter(centerView);
    }

    private BorderPane getTopView() {
        super.setButtonStyle(btnMainView);
        btnMainView.setOnAction(e -> INavigation.showPrimary(stage));
        super.setButtonStyle(btnDrawRound);
        btnDrawRound.setOnAction(e -> drawRound());
        fieldDiameter.setPromptText("Diameter");
        fieldDiameter.setMaxWidth(200);
        fieldDiameter.setTextFormatter(new TextFormatter<>(super.maxValFilter));
        BorderPane topPane = new BorderPane();
        BorderPane.setAlignment(btnMainView, Pos.TOP_LEFT);
        topPane.setLeft(btnMainView);
        BorderPane.setAlignment(fieldDiameter, Pos.TOP_CENTER);
        topPane.setTop(fieldDiameter);
        BorderPane.setAlignment(btnDrawRound, Pos.TOP_RIGHT);
        topPane.setRight(btnDrawRound);
        return topPane;
    }

    private void drawRound(){
        Canvas canvas = new Canvas(201, 201);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        PixelWriter pw = gc.getPixelWriter();
        Color lineColor = Color.WHITE;
        try {
            new RoundDrawer(pw, lineColor, Integer.parseInt(fieldDiameter.getText()));
        } catch (NumberFormatException ignored) {}
        super.centerPane.setCenter(canvas);
    }

    @Override
    public void showScene() {
        stage.setScene(new Scene(mainPane));
        stage.show();
    }
}
