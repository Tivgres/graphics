package me.tivgres.drawer.polygon;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.SnapshotResult;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import me.tivgres.base.BaseView;
import me.tivgres.drawer.INavigation;

import java.util.ArrayList;
import java.util.List;

public class PolygonFillView extends BaseView {

    private INavigation INavigation;
    private Stage stage;
    private BorderPane mainPane = new BorderPane();
    private BorderPane centerPane;
    private TextField fieldCoordinates = new TextField();
    private Button
            btnMainView = new Button("<"),
            btnFillPolygon = new Button("Fill"),
            btnLoadDemoFirst = new Button("Demo1"),
            btnLoadDemoSecond = new Button("Demo2");
    private String
            demoCoordinatesFirst = "0,0;195,5;185,199;155,130;130,199;40,140",
            demoCoordinatesSecond = "0,100;50,70;100,100;150,60;199,100;140,140;100,150;55,199;40,140";

    public PolygonFillView(Stage stage, INavigation INavigation) {
        this.INavigation = INavigation;
        this.stage = stage;
        prepareWorkspace();
        showScene();
    }

    @Override
    protected void prepareWorkspace() {
        mainPane.setPadding(new Insets(25));
        mainPane.setBackground(super.getBackground());
        BorderPane topView = getTopView();
        BorderPane.setAlignment(topView, Pos.TOP_CENTER);
        mainPane.setTop(topView);
        BorderPane centerView = super.getCenterView();
        BorderPane.setAlignment(centerView, Pos.CENTER);
        mainPane.setCenter(centerView);
    }

    private BorderPane getTopView() {
        super.setButtonStyle(btnMainView);
        btnMainView.setOnAction(e -> INavigation.showPrimary(stage));
        VBox vBox = new VBox(20);
        super.setButtonStyle(btnFillPolygon);
        btnFillPolygon.setOnAction(e -> fillPolygon());
        super.setButtonStyle(btnLoadDemoFirst);
        btnLoadDemoFirst.setOnAction(e -> loadDemo(1));
        super.setButtonStyle(btnLoadDemoSecond);
        btnLoadDemoSecond.setOnAction(e -> loadDemo(2));
        vBox.getChildren().addAll(btnFillPolygon, btnLoadDemoFirst, btnLoadDemoSecond);
        fieldCoordinates.setPromptText("Coordinates likes: \"x0,y0;x1,y1...xN,yN\"");
        fieldCoordinates.setMaxWidth(400);
        BorderPane topPane = new BorderPane();
        BorderPane.setAlignment(btnMainView, Pos.TOP_LEFT);
        topPane.setLeft(btnMainView);
        BorderPane.setAlignment(fieldCoordinates, Pos.TOP_CENTER);
        topPane.setTop(fieldCoordinates);
        BorderPane.setAlignment(vBox, Pos.TOP_RIGHT);
        topPane.setRight(vBox);
        return topPane;
    }

    private void loadDemo(int num) {
        switch (num) {
            case 1:
                fieldCoordinates.setText(demoCoordinatesFirst);
                break;
            case 2:
                fieldCoordinates.setText(demoCoordinatesSecond);
                break;
        }
        fillPolygon();
    }

    private void fillPolygon() {
        try {
            String[] coordinates = fieldCoordinates.getText().split(";");
            List<Integer>
                    xList = new ArrayList<>(),
                    yList = new ArrayList<>();
            for (String coordinate : coordinates) {
                String[] tempCoordinates = coordinate.split(",");
                xList.add(Integer.parseInt(tempCoordinates[0]));
                yList.add(Integer.parseInt(tempCoordinates[1]));
            }
            Canvas canvas = new Canvas(200, 200);
            GraphicsContext gc = canvas.getGraphicsContext2D();
            PixelWriter pw = gc.getPixelWriter();
            Color lineColor = Color.RED;
            PolygonDrawer pd = new PolygonDrawer(pw, lineColor, xList, yList);
            pd.drawPolygon();
            super.centerPane.setCenter(canvas);
            WritableImage image = new WritableImage(200,200);
            SnapshotParameters sp = new SnapshotParameters();
            canvas.snapshot(sp, image);
            PixelReader pr = image.getPixelReader();
            pd.fillPolygon(pr, (int) canvas.getWidth(), (int) canvas.getHeight());

        } catch (Exception ignored) {
            ignored.fillInStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Wrong input!");
            alert.showAndWait();
        }
    }


    @Override
    public void showScene() {
        mainPane.setPrefWidth(1000);
        mainPane.setPrefHeight(700);
        stage.setScene(new Scene(mainPane));
        stage.show();
    }
}
