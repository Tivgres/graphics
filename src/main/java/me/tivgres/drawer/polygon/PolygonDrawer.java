package me.tivgres.drawer.polygon;

import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.paint.Color;
import me.tivgres.drawer.line.LineDrawer;

import java.util.List;

class PolygonDrawer {

    private PixelWriter pw;
    private Color color;
    private List<Integer> xCoordinates, yCoordinates;

    PolygonDrawer(PixelWriter pw, Color color,
                  List<Integer> xCoordinates, List<Integer> yCoordinates){
        this.pw = pw;
        this.color = color;
        this.xCoordinates = xCoordinates;
        this.yCoordinates = yCoordinates;
    }

    void drawPolygon(){
        for (int i = 0; i < xCoordinates.size(); i++) {
            if (i != xCoordinates.size() -1){
                new LineDrawer(pw, color,
                        xCoordinates.get(i), xCoordinates.get(i + 1),
                        yCoordinates.get(i), yCoordinates.get(i + 1));
            } else {
                new LineDrawer(pw, color,
                        xCoordinates.get(i), xCoordinates.get(0),
                        yCoordinates.get(i), yCoordinates.get(0));
            }
        }
    }

    void fillPolygon(PixelReader pr, int width, int height){
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (color.equals(pr.getColor(x, y))) {
                    checkLine(pr, x, y, width);
                }
            }
        }
    }

    private void checkLine(PixelReader pr, int x, int y, int width){
        for (int x1 = x + 1; x1 < width; x1++) {
            if (color.equals(pr.getColor(x1, y))){
                fillLine(x, x1, y);
                break;
            }
        }
    }

    private void fillLine(int x0, int x1, int y){
        for (int x = x0; x < x1; x++) {
            pw.setColor(x, y, color);
        }
    }
}
