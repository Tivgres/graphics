package me.tivgres.drawer;

import javafx.stage.Stage;

public interface INavigation {

    void showPrimary(Stage stage);

    void drawLine(Stage stage);

    void drawRound(Stage stage);

    void fillRound(Stage stage);

}
