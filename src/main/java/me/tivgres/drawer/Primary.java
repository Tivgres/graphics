package me.tivgres.drawer;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import me.tivgres.base.BaseView;

public class Primary extends BaseView {

    private INavigation INavigation;
    private Stage stage;
    private HBox centerLayout;
    private Button btnLineDrawer, btnRoundDrawer, btnRoundFill;

    public Primary(Stage stage, INavigation INavigation){
        this.INavigation = INavigation;
        this.stage = stage;
        prepareWorkspace();
        showScene();
    }

    @Override
    protected void prepareWorkspace() {
        centerLayout = new HBox(50);
        btnLineDrawer = new Button();
        setBtnText(btnLineDrawer, "Line drawer");
        setBtnSize(btnLineDrawer, 250);
        super.setButtonStyle(btnLineDrawer);
        btnLineDrawer.setOnAction(e -> INavigation.drawLine(stage));
        btnRoundDrawer = new Button();
        setBtnText(btnRoundDrawer, "Round drawer");
        setBtnSize(btnRoundDrawer, 250);
        super.setButtonStyle(btnRoundDrawer);
        btnRoundDrawer.setOnAction(e -> INavigation.drawRound(stage));
        btnRoundFill = new Button();
        setBtnText(btnRoundFill, "Polygon fill");
        setBtnSize(btnRoundFill, 250);
        super.setButtonStyle(btnRoundFill);
        btnRoundFill.setOnAction(e -> INavigation.fillRound(stage));
        centerLayout.getChildren().addAll(btnLineDrawer, btnRoundDrawer, btnRoundFill);
    }

    private void setBtnText(Button btn, String text){
        btn.setText(text);
        btn.setFont(super.fontMain);
    }

    private void setBtnSize(Button btn, int size){
        btn.setPrefSize(size, size);
    }

    @Override
    public void showScene() {
        centerLayout.setAlignment(Pos.CENTER);
        BorderPane bp = new BorderPane();
        BorderPane.setAlignment(centerLayout, Pos.CENTER);
        bp.setCenter(centerLayout);
        addAuthor(bp);
        bp.setPrefWidth(1000);
        bp.setPrefHeight(700);
        bp.setBackground(super.getBackground());
        stage.setScene(new Scene(bp));
        stage.show();
    }

    private void addAuthor(BorderPane bp){
        Label label = new Label("Created by Tivgres mr.serg.vit@gmail.com");
        label.setFont(super.fontSecondary);
        label.setTextFill(Color.web("#f2f2f2"));
        BorderPane.setAlignment(label, Pos.BOTTOM_CENTER);
        bp.setBottom(label);
    }
}
