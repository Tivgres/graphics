package me.tivgres.drawer.line;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.image.PixelWriter;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.control.TextFormatter.Change;
import me.tivgres.base.BaseView;
import me.tivgres.drawer.INavigation;


public class LineDrawerView extends BaseView {

    private INavigation INavigation;
    private Stage stage;
    private BorderPane mainPane = new BorderPane();
    private TextField
            fieldX0 = new TextField(),
            fieldX1 = new TextField(),
            fieldY0 = new TextField(),
            fieldY1 = new TextField();
    private Button
            btnMainView = new Button("<"),
            btnDrawLine = new Button("Draw");

    public LineDrawerView(Stage stage, INavigation INavigation) {
        this.INavigation = INavigation;
        this.stage = stage;
        prepareWorkspace();
        showScene();
    }

    @Override
    protected void prepareWorkspace() {
        mainPane.setPadding(new Insets(25));
        mainPane.setBackground(super.getBackground());
        HBox topView = getTopView();
        BorderPane.setAlignment(topView, Pos.TOP_CENTER);
        mainPane.setTop(topView);
        BorderPane centerView = super.getCenterView();
        BorderPane.setAlignment(centerView, Pos.CENTER);
        mainPane.setCenter(centerView);
    }

    private HBox getTopView() {
        super.setButtonStyle(btnMainView);
        btnMainView.setOnAction(e -> INavigation.showPrimary(stage));
        super.setButtonStyle(btnDrawLine);
        btnDrawLine.setOnAction(e -> drawLine());
        fieldX0.setPromptText("x0");
        fieldX0.setTextFormatter(new TextFormatter<>(super.maxValFilter));
        fieldX1.setPromptText("x1");
        fieldX1.setTextFormatter(new TextFormatter<>(super.maxValFilter));
        fieldY0.setPromptText("y0");
        fieldY0.setTextFormatter(new TextFormatter<>(super.maxValFilter));
        fieldY1.setPromptText("y1");
        fieldY1.setTextFormatter(new TextFormatter<>(super.maxValFilter));
        HBox hBox = new HBox(25);
        hBox.getChildren().addAll(btnMainView, fieldX0, fieldX1, fieldY0, fieldY1, btnDrawLine);
        return hBox;
    }

    private void drawLine() {
        Canvas canvas = new Canvas(200, 200);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        PixelWriter pw = gc.getPixelWriter();
        Color lineColor = Color.WHITE;
        try {
            new LineDrawer(pw, lineColor,
                    Integer.parseInt(fieldX0.getText()),
                    Integer.parseInt(fieldX1.getText()),
                    Integer.parseInt(fieldY0.getText()),
                    Integer.parseInt(fieldY1.getText()));
        } catch (NumberFormatException ignored) {}
        super.centerPane.setCenter(canvas);
    }



    @Override
    public void showScene() {
        stage.setScene(new Scene(mainPane));
        stage.show();
    }
}
