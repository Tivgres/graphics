package me.tivgres.drawer.line;

import javafx.scene.image.PixelWriter;
import javafx.scene.paint.Color;

public class LineDrawer {

    public LineDrawer(PixelWriter pw, Color color, int x0, int x1, int y0, int y1) {
        int d = 0;
        int dx = Math.abs(x1 - x0);
        int dy = Math.abs(y1 - y0);
        int dx2 = 2 * dx;
        int dy2 = 2 * dy;
        int ix = x0 < x1 ? 1 : -1;
        int iy = y0 < y1 ? 1 : -1;
        int x = x0;
        int y = y0;
        if (dx >= dy) {
            while (true) {
                pw.setColor(x, y, color);
                if (x == x1)
                    break;
                x += ix;
                d += dy2;
                if (d > dx) {
                    y += iy;
                    d -= dx2;
                }
            }
        } else {
            while (true) {
                pw.setColor(x, y, color);
                if (y == y1)
                    break;
                y += iy;
                d += dx2;
                if (d > dy) {
                    x += ix;
                    d -= dy2;
                }
            }
        }
    }
}