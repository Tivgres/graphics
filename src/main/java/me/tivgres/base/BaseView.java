package me.tivgres.base;

import javafx.scene.control.Button;
import javafx.scene.control.TextFormatter;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;

import java.util.function.UnaryOperator;

public abstract class BaseView {

    protected UnaryOperator<TextFormatter.Change> maxValFilter;
    protected BorderPane centerPane;
    protected Font fontMain, fontSecondary, fontThird;

    protected BaseView(){
        fontMain = Font.loadFont(getClass().getResourceAsStream("/fonts/Roboto-Medium.ttf"), 30);
        fontSecondary = Font.loadFont(getClass().getResourceAsStream("/fonts/Roboto-Regular.ttf"), 16);
        fontThird = Font.loadFont(getClass().getResourceAsStream("/fonts/Roboto-Light.ttf"), 14);
        String regexMaxVal = "^(([01]?[0-9]{0,2}))";
        maxValFilter = c -> {
            String text = c.getControlNewText();
            if (text.matches(regexMaxVal)) {
                return c;
            } else {
                return null;
            }
        };
    }

    protected BorderPane getCenterView() {
        centerPane = new BorderPane();
        return centerPane;
    }

    protected Background getBackground() {
        return new Background(
                new BackgroundImage(
                        new Image(getClass().getResourceAsStream("/pics/bg.png")),
                        BackgroundRepeat.NO_REPEAT,
                        BackgroundRepeat.NO_REPEAT,
                        BackgroundPosition.CENTER,
                        new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO,
                                false, false, true, true)));
    }

    protected void setButtonStyle(Button btn){
        btn.getStylesheets().add(getClass().getResource("/css/jfx-button.css").toExternalForm());
    }

    protected abstract void prepareWorkspace();

    public abstract void showScene();

}
